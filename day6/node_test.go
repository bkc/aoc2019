package main

import (
	"bufio"
	"os"
	"testing"
)

func BenchmarkNodeParents(b *testing.B) {
	fp, err := os.Open("input")
	if err != nil {
		panic(err)
	}
	defer fp.Close()

	com := parseOrbits(bufio.NewReader(fp))

	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			com.parents(0)
		}
	})
}

func BenchmarkNodeAncestors(b *testing.B) {
	fp, err := os.Open("input")
	if err != nil {
		panic(err)
	}
	defer fp.Close()

	com := parseOrbits(bufio.NewReader(fp))

	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			com.ancestors()
		}
	})
}

func TestNodeParents(t *testing.T) {
	t.Parallel()
	com := &Node{name: "COM"}
	node := com.Birth("A").Birth("B").Birth("C")

	if n := node.parents(0); n != 3 {
		t.Errorf("parents should be %d, was %d", 3, n)
	}
}

func TestNodeAncestors(t *testing.T) {
	t.Parallel()
	com := &Node{name: "COM"}
	node := com.Birth("A").Birth("B").Birth("C")

	if n := node.ancestors(); n != 3 {
		t.Errorf("ancestors should be %d, was %d", 3, n)
	}
}
