package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func parseOrbits(r *bufio.Reader) *Node {
	var (
		parent string
		child  string
		err    error
		buf    []byte
		orphan []*Node
	)

	com := &Node{name: "COM"}

	for err == nil {
		buf, _, err = r.ReadLine()
		if err != nil {
			if err == io.EOF {
				break
			}
			panic(err)
		}
		if _, err = fmt.Sscanf(string(buf), "%3s)%3s", &parent, &child); err != nil {
			if err != io.EOF {
				panic(err)
			}
		}
		p := com.Get(parent)
		if p == nil {
			for _, l := range orphan {
				p = l.Get(parent)
				if p != nil {
					break
				}
			}
			if p == nil {
				p = &Node{name: parent}
				orphan = append(orphan, p)
			}
		}
		lost := false
		for idx, c := range orphan {
			if c.name == child {
				lost = true
				p.Adopt(c)
				orphan = append(orphan[:idx], orphan[idx+1:]...)
			}
		}
		if !lost {
			p.Birth(child)
		}
	}

	if len(orphan) > 0 {
		panic(fmt.Sprintf("orphan objects: %d", len(orphan)))
	}

	return com
}

func main() {
	fp, err := os.Open("input")
	if err != nil {
		panic(err)
	}
	defer fp.Close()

	com := parseOrbits(bufio.NewReader(fp))

	// Part 1
	orbits := 0
	com.Walk(func(node *Node) {
		orbits = orbits + node.Parents()
	})
	fmt.Println(orbits)

	// part 2
	you := com.Get("YOU")
	root := you.parent
	transfers := 0
	for ; root.Get("SAN") == nil; root = root.parent {
		transfers++
	}
	fmt.Println(transfers + root.LenTo("SAN") - 1)
}
