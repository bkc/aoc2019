package main

import (
	"fmt"
)

type Node struct {
	name   string
	child  []*Node
	parent *Node
}

func (n *Node) Adopt(child *Node) {
	child.parent = n
	n.child = append(n.child, child)
}

func (n *Node) Birth(name string) *Node {
	c := &Node{name: name}
	n.Adopt(c)
	return c
}

func (n *Node) Get(name string) *Node {
	if name == n.name {
		return n
	}
	for _, c := range n.child {
		if c.name == name {
			return c
		}
		if found := c.Get(name); found != nil {
			return found
		}
	}
	return nil
}

func (n *Node) LenTo(name string) int {
	if n.Get(name) == nil {
		return 0
	}
	for _, c := range n.child {
		if c.Get(name) == nil {
			continue
		}
		return c.LenTo(name) + 1
	}
	return 0
}

func (n *Node) Parents() int {
	if n == nil {
		panic("FUCK")
	}
	d := n.ancestors()
	return d
}

// ancestors returns how many ancestors this node has.
//  uses a for-loop
func (n *Node) ancestors() int {
	node, iter := n.parent, 0
	for node != nil {
		node = node.parent
		iter++
	}

	return iter
}

// parents returns how many ancestors this node has.
//  uses tail-recursion
func (n *Node) parents(iter int) int {
	if n.parent == nil {
		return iter
	}
	return n.parent.parents(iter + 1)
}

func (n *Node) Print() {
	n.Walk(func(node *Node) {
		if node.parent != nil {
			fmt.Printf("%s)%s\n", node.parent.name, node.name)
		}
	})
}

func (n *Node) WalkLeafs(f func(*Node)) {
	if len(n.child) == 0 {
		f(n)
	}
	for _, c := range n.child {
		c.WalkLeafs(f)
	}
}

func (n *Node) Walk(f func(*Node)) {
	f(n)
	for _, c := range n.child {
		c.Walk(f)
	}
}
