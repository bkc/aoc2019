package main

import (
	"fmt"
	"strconv"
)

const min = 264793
const max = 803935

func mustInt(i string) int {
	o, e := strconv.Atoi(i)
	if e != nil {
		panic(e)
	}
	return o
}

func main() {
	amount := 0
	for i := min; i < max; i++ {
		s := fmt.Sprintf("%d", i)

		if checkReducingNumber(s) {
			continue
		}

		if checkPart2(s) {
			amount++
		}
	}
	fmt.Println(amount)
}

func checkPart2(s string) bool {
	count := [10]int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
	for _, c := range s {
		a := mustInt(string(c))
		count[a]++
	}
	for _, c := range count {
		if c == 2 {
			return true
		}
	}
	return false
}

func checkReducingNumber(s string) bool {
	for i, c := range s {
		a := mustInt(string(c))
		// check for decreasing number
		for j := i; j > 0; j-- {
			b := mustInt(string(s[j-1]))
			if a < b {
				return true
			}
		}
	}
	return false
}

func checkDouble(s string) bool {
	double := false
	for i := 1; i < len(s); i++ {
		a := mustInt(s[i-1 : i])
		b := mustInt(s[i : i+1])
		if a == b {
			double = true
		}
	}
	return double
}
