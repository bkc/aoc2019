package main

import (
	"fmt"
	"os"
)

var input = []int{1, 0, 0, 3, 1, 1, 2, 3, 1, 3, 4, 3, 1, 5, 0, 3, 2, 1, 9, 19, 1, 10, 19, 23, 2, 9, 23, 27, 1, 6, 27, 31, 2, 31, 9, 35, 1, 5, 35, 39, 1, 10, 39, 43, 1, 10, 43, 47, 2, 13, 47, 51, 1, 10, 51, 55, 2, 55, 10, 59, 1, 9, 59, 63, 2, 6, 63, 67, 1, 5, 67, 71, 1, 71, 5, 75, 1, 5, 75, 79, 2, 79, 13, 83, 1, 83, 5, 87, 2, 6, 87, 91, 1, 5, 91, 95, 1, 95, 9, 99, 1, 99, 6, 103, 1, 103, 13, 107, 1, 107, 5, 111, 2, 111, 13, 115, 1, 115, 6, 119, 1, 6, 119, 123, 2, 123, 13, 127, 1, 10, 127, 131, 1, 131, 2, 135, 1, 135, 5, 0, 99, 2, 14, 0, 0}

func main() {
	// part 1
	fmt.Println(runMachine(12, 2))

	// part 2
	for noun := 0; noun <= 99; noun++ {
		for verb := 0; verb <= 99; verb++ {
			test := runMachine(noun, verb)
			if test == 19690720 {
				fmt.Println(100*noun + verb)
				os.Exit(0)
			}
		}
	}
}
func runMachine(noun, verb int) int {
	fmt.Printf("%d:%d\n", noun, verb)
	state := make([]int, len(input))
	copy(state, input)
	state[1] = noun
	state[2] = verb

	for ip := 0; ip < len(state); ip = ip + 4 {
		if !iterState(state, ip) {
			break
		}
	}

	return state[0]
}

func iterState(state []int, ip int) bool {
	switch state[ip] {
	case 1:
		state[state[ip+3]] = state[state[ip+1]] + state[state[ip+2]]
	case 2:
		state[state[ip+3]] = state[state[ip+1]] * state[state[ip+2]]
	case 99:
		return false
	}
	return true
}
