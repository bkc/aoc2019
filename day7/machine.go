package main

// OpCode defines our opcodes...
type OpCode int

const (
	OpADD   OpCode = 1
	OpMUL   OpCode = 2
	OpLOAD  OpCode = 3
	OpSTORE OpCode = 4
	OpJNZ   OpCode = 5
	OpJZ    OpCode = 6
	OpLT    OpCode = 7
	OpEQ    OpCode = 8
	OpHALT  OpCode = 99
)

// Machine is a state-machine that runs IntCode
type Machine struct {
	code   []int
	state  []int
	ip     int
	input  <-chan int
	output chan int
}

// NewMachine creates a new machine
//  `input` is copied into the machines state
func NewMachine(input []int) *Machine {
	machine := &Machine{ip: 0, code: make([]int, len(input)), output: make(chan int, 2)}
	copy(machine.code, input)
	return machine
}

// Run the state-machine. This is idempotent and can be ran over and over again
func (m *Machine) Run(input <-chan int) {
	// Copy the code to the state
	if len(m.code) != len(m.state) {
		m.state = make([]int, len(m.code))
	}
	copy(m.state, m.code)

	m.ReRun(input)
}

// ReRun the state-machine. This does not reset the state...
func (m *Machine) ReRun(input <-chan int) {
	// Copy the code to the state
	m.ip = 0

	// Setup the input data
	m.input = input

	// Run the state-machine
	for m.iterate() {
	}
}

// Output returns the output of the state-machine
func (m *Machine) Output() chan int {
	return m.output
}

func (m *Machine) iterate() bool {
	op, mod1, mod2, mod3 := parseInstruction(m.state[m.ip])
	switch op {
	case OpADD:
		m.setMod(3, mod3, m.getMod(1, mod1)+m.getMod(2, mod2))
		m.ip = m.ip + 4
	case OpMUL:
		m.setMod(3, mod3, m.getMod(1, mod1)*m.getMod(2, mod2))
		m.ip = m.ip + 4
	case OpLOAD:
		input := <-m.input
		m.setMod(1, mod1, input)
		m.ip = m.ip + 2
	case OpSTORE:
		m.output <- m.getMod(1, mod1)
		m.ip = m.ip + 2
	case OpJNZ:
		if m.getMod(1, mod1) != 0 {
			m.ip = m.getMod(2, mod2)
		} else {
			m.ip = m.ip + 3
		}
	case OpJZ:
		if m.getMod(1, mod1) == 0 {
			m.ip = m.getMod(2, mod2)
		} else {
			m.ip = m.ip + 3
		}
	case OpLT:
		if m.getMod(1, mod1) < m.getMod(2, mod2) {
			m.setMod(3, mod3, 1)
		} else {
			m.setMod(3, mod3, 0)
		}
		m.ip = m.ip + 4
	case OpEQ:
		if m.getMod(1, mod1) == m.getMod(2, mod2) {
			m.setMod(3, mod3, 1)
		} else {
			m.setMod(3, mod3, 0)
		}
		m.ip = m.ip + 4
	case OpHALT:
		return false
	}
	return true
}

func (m *Machine) setMod(offset, mod, value int) {
	if mod == 0 {
		m.state[m.state[m.ip+offset]] = value
	} else {
		m.state[m.ip+offset] = value
	}
}

func (m *Machine) getMod(offset, mod int) int {
	if mod == 0 {
		return m.state[m.state[m.ip+offset]]
	}
	return m.state[m.ip+offset]
}

func parseInstruction(in int) (op OpCode, mod1, mod2, mod3 int) {
	op = OpCode(in % 100)
	if in >= 100 {
		mod1 = int(in/100) % 10
	}
	if in >= 1000 {
		mod2 = int(in/1000) % 10
	}
	if in >= 10000 {
		mod3 = int(in/10000) % 10
	}
	return
}
