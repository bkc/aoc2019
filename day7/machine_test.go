package main

import (
	"fmt"
	"testing"
)

func TestParseInstruction(t *testing.T) {
	tests := []struct {
		name     string
		input    int
		wantOp   OpCode
		wantMod1 int
		wantMod2 int
		wantMod3 int
	}{
		{
			name:   "2",
			input:  2,
			wantOp: 2,
		},
		{
			name:     "102",
			input:    102,
			wantOp:   2,
			wantMod1: 1,
		},
		{
			name:     "1002",
			input:    1002,
			wantOp:   2,
			wantMod2: 1,
		},
		{
			name:     "11002",
			input:    11002,
			wantOp:   2,
			wantMod2: 1,
			wantMod3: 1,
		},
		{
			name:     "11102",
			input:    11102,
			wantOp:   2,
			wantMod1: 1,
			wantMod2: 1,
			wantMod3: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotOp, gotMod1, gotMod2, gotMod3 := parseInstruction(tt.input)
			if gotOp != tt.wantOp {
				t.Errorf("parseInstruction() gotOp = %v, want %v", gotOp, tt.wantOp)
			}
			if gotMod1 != tt.wantMod1 {
				t.Errorf("parseInstruction() gotMod1 = %v, want %v", gotMod1, tt.wantMod1)
			}
			if gotMod2 != tt.wantMod2 {
				t.Errorf("parseInstruction() gotMod2 = %v, want %v", gotMod2, tt.wantMod2)
			}
			if gotMod3 != tt.wantMod3 {
				t.Errorf("parseInstruction() gotMod3 = %v, want %v", gotMod3, tt.wantMod3)
			}
		})
	}
}

func TestIterate(t *testing.T) {
	type args struct {
		state []int
		ip    int
	}
	tests := []struct {
		name      string
		args      args
		wantIp    int
		wantRet   bool
		wantState []int
	}{
		{
			name:      "halt",
			args:      args{state: []int{99}},
			wantIp:    0,
			wantRet:   false,
			wantState: []int{99},
		},
		{
			name:      "eq test",
			args:      args{state: []int{8, 5, 6, 5, 99, 8, 8}},
			wantIp:    4,
			wantRet:   true,
			wantState: []int{8, 5, 6, 5, 99, 1, 8},
		},
		{
			name:      "add test",
			args:      args{state: []int{1, 1, 1, 3, 99}},
			wantIp:    4,
			wantRet:   true,
			wantState: []int{1, 1, 1, 2, 99},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &Machine{state: tt.args.state, ip: tt.args.ip}
			got := m.iterate()
			if m.ip != tt.wantIp {
				t.Errorf("iterate() should set ip to %v, got %v", tt.wantIp, m.ip)
			}
			if got != tt.wantRet {
				t.Errorf("iterate() got1 = %v, want %v", got, tt.wantRet)
			}
			if fmt.Sprintf("%v", m.state) != fmt.Sprintf("%v", tt.wantState) {
				t.Errorf("iterate() has wrong state: want %v, got %v", tt.wantState, m.state)
			}
		})
	}
}

func TestMachineRun(t *testing.T) {
	tests := []struct {
		name      string
		inData    int
		code      []int
		hasOutput bool
		output    int
		outState  []int
	}{
		{
			name:     "day2 tutorial",
			inData:   0,
			code:     []int{1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50},
			outState: []int{3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50},
		},
		{
			name:     "day2 example 1",
			code:     []int{1, 0, 0, 0, 99},
			outState: []int{2, 0, 0, 0, 99},
		},
		{
			name:     "day2 example 2",
			code:     []int{2, 3, 0, 3, 99},
			outState: []int{2, 3, 0, 6, 99},
		},
		{
			name:     "day2 example 3",
			code:     []int{2, 4, 4, 5, 99, 0},
			outState: []int{2, 4, 4, 5, 99, 9801},
		},
		{
			name:     "day2 example 4",
			code:     []int{1, 1, 1, 4, 99, 5, 6, 0, 99},
			outState: []int{30, 1, 1, 4, 2, 5, 6, 0, 99},
		},
		{
			name:      "day 5 example 1 - true",
			inData:    7,
			code:      []int{3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8},
			hasOutput: true,
			output:    0,
		},
		{
			name:      "day 5 example 1 - false",
			inData:    8,
			code:      []int{3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8},
			hasOutput: true,
			output:    1,
		},
		{
			name:      "day 5 example 2 - true",
			inData:    7,
			code:      []int{3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8},
			hasOutput: true,
			output:    1,
		},
		{
			name:      "day 5 example 2 - true",
			inData:    8,
			code:      []int{3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8},
			hasOutput: true,
			output:    0,
		},
		{
			name:      "day 5 example 3 - true",
			inData:    8,
			code:      []int{3, 3, 1108, -1, 8, 3, 4, 3, 99},
			hasOutput: true,
			output:    1,
		},
		{
			name:      "day 5 example 3 - false",
			inData:    7,
			code:      []int{3, 3, 1108, -1, 8, 3, 4, 3, 99},
			hasOutput: true,
			output:    0,
		},
		{
			name:      "day 5 example 4 - true",
			inData:    7,
			code:      []int{3, 3, 1107, -1, 8, 3, 4, 3, 99},
			hasOutput: true,
			output:    1,
		},
		{
			name:      "day 5 example 4 - false",
			inData:    8,
			code:      []int{3, 3, 1107, -1, 8, 3, 4, 3, 99},
			hasOutput: true,
			output:    0,
		},
		{
			name:      "day 5 big example - less than",
			inData:    7,
			code:      []int{3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31, 1106, 0, 36, 98, 0, 0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104, 999, 1105, 1, 46, 1101, 1000, 1, 20, 4, 20, 1105, 1, 46, 98, 99},
			hasOutput: true,
			output:    999,
		},
		{
			name:      "day 5 big example - equal",
			inData:    8,
			code:      []int{3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31, 1106, 0, 36, 98, 0, 0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104, 999, 1105, 1, 46, 1101, 1000, 1, 20, 4, 20, 1105, 1, 46, 98, 99},
			hasOutput: true,
			output:    1000,
		},
		{
			name:      "day 5 big example - bigger than",
			inData:    9,
			code:      []int{3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31, 1106, 0, 36, 98, 0, 0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104, 999, 1105, 1, 46, 1101, 1000, 1, 20, 4, 20, 1105, 1, 46, 98, 99},
			hasOutput: true,
			output:    1001,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := NewMachine(tt.code)
			in := make(chan int, 1)
			in <- tt.inData
			m.Run(in)
			if tt.hasOutput {
				if foo := <-m.Output(); foo != tt.output {
					t.Errorf("runMachine has wrong output: want %v, got %v", tt.output, foo)
				}
			}

			if len(tt.outState) > 0 && fmt.Sprintf("%v", tt.outState) != fmt.Sprintf("%v", m.state) {
				t.Errorf("runMachine has wrong state: want %v, got %v", tt.outState, m.state)
			}
		})
	}
}
