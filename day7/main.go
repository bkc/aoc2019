package main

import (
	"fmt"
	"sync"
)

var puzzle = []int{3, 8, 1001, 8, 10, 8, 105, 1, 0, 0, 21, 34, 47, 72, 81, 94, 175, 256, 337, 418, 99999, 3, 9, 102, 3, 9, 9, 1001, 9, 3, 9, 4, 9, 99, 3, 9, 101, 4, 9, 9, 1002, 9, 5, 9, 4, 9, 99, 3, 9, 1001, 9, 5, 9, 1002, 9, 5, 9, 1001, 9, 2, 9, 1002, 9, 5, 9, 101, 5, 9, 9, 4, 9, 99, 3, 9, 102, 2, 9, 9, 4, 9, 99, 3, 9, 1001, 9, 4, 9, 102, 4, 9, 9, 4, 9, 99, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 99, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 99, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 99, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 101, 1, 9, 9, 4, 9, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 99, 3, 9, 102, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 2, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 101, 2, 9, 9, 4, 9, 3, 9, 1001, 9, 1, 9, 4, 9, 3, 9, 1002, 9, 2, 9, 4, 9, 99}

func main() {
	// part 1
	maxThrust := 0
	phaseLoop([]int{0, 1, 2, 3, 4}, func(curPhases []int) {
		output := fireThruster(puzzle, curPhases)
		if output > maxThrust {
			maxThrust = output
		}
	})
	fmt.Println(maxThrust)

	// part 2
	maxThrust = 0
	phaseLoop([]int{5, 6, 7, 8, 9}, func(curPhases []int) {
		output := fireThruster2(puzzle, curPhases)
		if output > maxThrust {
			maxThrust = output
		}
	})
	fmt.Println(maxThrust)
}

func phaseLoop(phases []int, fn func(curPhases []int)) {
	for _, a := range phases {
		for _, b := range phases {
			if b == a {
				continue
			}
			for _, c := range phases {
				if c == a || c == b {
					continue
				}
				for _, d := range phases {
					if d == a || d == b || d == c {
						continue
					}
					for _, e := range phases {
						if e == a || e == b || e == c || e == d {
							continue
						}
						fn([]int{a, b, c, d, e})
					}
				}
			}
		}
	}
}

func fireThruster2(code, phases []int) int {
	var amps []*Machine

	// Create one machine for each phase
	for i := range phases {
		if len(amps) == i {
			amps = append(amps, NewMachine(code))
		}
	}
	amps[4].output <- phases[0]
	amps[0].output <- phases[1]
	amps[1].output <- phases[2]
	amps[2].output <- phases[3]
	amps[3].output <- phases[4]
	wg := &sync.WaitGroup{}
	wg.Add(5)
	go func(wg *sync.WaitGroup) {
		amps[0].Run(amps[4].Output())
		wg.Done()
	}(wg)
	go func(wg *sync.WaitGroup) {
		amps[1].Run(amps[0].Output())
		wg.Done()
	}(wg)
	go func(wg *sync.WaitGroup) {
		amps[2].Run(amps[1].Output())
		wg.Done()
	}(wg)
	go func(wg *sync.WaitGroup) {
		amps[3].Run(amps[2].Output())
		wg.Done()
	}(wg)
	go func(wg *sync.WaitGroup) {
		amps[4].Run(amps[3].Output())
		wg.Done()
	}(wg)

	amps[4].output <- 0

	wg.Wait()

	return <-amps[4].Output()
}

func fireThruster(code, phases []int) int {
	amp := NewMachine(code)

	in := make(chan int, 2)
	out := 0
	for _, phase := range phases {
		in <- phase
		in <- out
		amp.Run(in)
		out = <-amp.Output()
	}
	return out
}
