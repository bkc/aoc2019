package main

import (
	"fmt"
	"testing"
)

func TestImage(t *testing.T) {
	tests := []struct {
		name   string
		input  []int
		w, h   int
		layers [][]int
	}{
		{
			name:  "day 8 - example 1",
			input: []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2},
			w:     3,
			h:     2,
			layers: [][]int{
				[]int{1, 2, 3, 4, 5, 6},
				[]int{7, 8, 9, 0, 1, 2},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			layers := image(tt.w, tt.h, tt.input)
			if len(layers) != len(tt.layers) {
				t.Errorf("# layers wrong: got %d, want %d", len(layers), len(tt.layers))
			}
			for i, layer := range layers {
				if fmt.Sprintf("%v", layer) != fmt.Sprintf("%v", tt.layers[i]) {
					fmt.Errorf("layer %d wrong: got %v, want %v", i, layer, layers[i])
				}
			}
		})
	}
}
