package main

import (
	"fmt"
	"math"
)

type Color int

const (
	Black       Color = 0
	White       Color = 1
	Transparent Color = 2
)

type Painting []Color

func (p Painting) Print(w, h int) {
	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			switch p[y*w+x] {
			case Transparent:
				fmt.Printf(" ")
			case White:
				fmt.Printf("A")
			case Black:
				fmt.Printf(" ")
			}
		}
		fmt.Printf("\n")
	}
}

func main() {

	layers := image(25, 6, puzzle)

	part1(layers)

	o := render(25, 6, layers)
	o.Print(25, 6)
}

func part1(layers [][]int) {
	minLayer := -1
	minZeroes := math.MaxInt64
	for i, layer := range layers {
		zeroes := 0
		for _, num := range layer {
			switch num {
			case 0:
				zeroes++
			}
		}
		if zeroes < minZeroes {
			minLayer = i
			minZeroes = zeroes
		}
	}

	ones := 0
	twos := 0
	for _, num := range layers[minLayer] {
		switch num {
		case 1:
			ones++
		case 2:
			twos++
		}
	}

	fmt.Println(ones * twos)
}

func render(w, h int, image [][]int) Painting {
	output := make(Painting, w*h)
	for i := range output {
		output[i] = Transparent
	}

	for l := range image {
		for x := 0; x < w; x++ {
			for y := 0; y < h; y++ {
				if output[x+y*w] == Transparent {
					output[x+y*w] = Color(image[l][x+y*w])
				}
			}
		}
	}

	return output
}

func image(w, h int, input []int) [][]int {
	if len(input)%(w*h) != 0 {
		panic("not a valid input")
	}
	layers := make([][]int, len(input)/(w*h))
	for i := range layers {
		layers[i] = make([]int, w*h)
		copy(layers[i], input[w*h*i:])
	}

	return layers
}
