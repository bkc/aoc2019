package main

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

// OpCode defines our opcodes...
type OpCode int

const (
	OpADD    OpCode = 1
	OpMUL    OpCode = 2
	OpLOAD   OpCode = 3
	OpSTORE  OpCode = 4
	OpJNZ    OpCode = 5
	OpJZ     OpCode = 6
	OpLT     OpCode = 7
	OpEQ     OpCode = 8
	OpSETREL OpCode = 9
	OpHALT   OpCode = 99
)

// Machine is a state-machine that runs IntCode
type Machine struct {
	code      []int64
	state     []int64
	relBase   int64
	ip        int64
	tempInput *int64
	// input     <-chan int64
	input  func() int64
	output chan int64
}

// NewMachine creates a new machine
//  `input` is copied into the machines state
func NewMachine(input []int64) *Machine {
	machine := &Machine{ip: 0, code: make([]int64, len(input)), output: make(chan int64)}
	copy(machine.code, input)
	return machine
}

// Run the state-machine. This is idempotent and can be ran over and over again
func (m *Machine) Run(input func() int64) {
	// Copy the code to the state
	if len(m.code) != len(m.state) {
		m.state = make([]int64, len(m.code))
	}
	copy(m.state, m.code)

	m.ReRun(input)
}

// ReRun the state-machine. This does not reset the state...
func (m *Machine) ReRun(input func() int64) {
	// Copy the code to the state
	m.ip = 0

	// Setup the input data
	m.input = input

	m.run()
}

func mustInt(str string) int64 {
	i, e := strconv.ParseInt(str, 10, 64)
	if e != nil {
		panic(e)
	}
	return i
}

func (m *Machine) run() {
	// Panic handler :D
	defer func() {
		if r := recover(); r != nil {
			if strings.Contains(fmt.Sprint(r), "index out of range") {
				re := regexp.MustCompile(`\[([0-9]+)\]`)
				matches := re.FindAllStringSubmatch(fmt.Sprint(r), 2)
				reNewSize := mustInt(matches[0][1]) + 1
				if reNewSize <= 0 {
					panic("no negative numbers ffs!")
				}
				newState := make([]int64, reNewSize*2)
				copy(newState, m.state)
				m.state = newState
				fmt.Printf("%s\n", r)
				m.run()
			} else {
				fmt.Printf("%s\n", r)
			}
		}

		// m.run()
	}()

	// Run the state-machine
	for m.iterate() {
	}
}

// Output returns the output of the state-machine
func (m *Machine) Output() chan int64 {
	return m.output
}

func (m *Machine) iterate() bool {
	op, mod1, mod2, mod3 := parseInstruction(m.state[m.ip])
	switch op {
	case OpADD:
		m.setMod(3, mod3, m.getMod(1, mod1)+m.getMod(2, mod2))
		m.ip = m.ip + 4
	case OpMUL:
		m.setMod(3, mod3, m.getMod(1, mod1)*m.getMod(2, mod2))
		m.ip = m.ip + 4
	case OpLOAD:
		var input int64
		if m.tempInput != nil {
			input = *m.tempInput
			m.tempInput = nil
		} else {
			input = m.input()
			m.tempInput = &input
		}
		m.setMod(1, mod1, input)
		m.ip = m.ip + 2
	case OpSTORE:
		m.output <- m.getMod(1, mod1)
		m.ip = m.ip + 2
	case OpJNZ:
		if m.getMod(1, mod1) != 0 {
			m.ip = m.getMod(2, mod2)
		} else {
			m.ip = m.ip + 3
		}
	case OpJZ:
		if m.getMod(1, mod1) == 0 {
			m.ip = m.getMod(2, mod2)
		} else {
			m.ip = m.ip + 3
		}
	case OpLT:
		if m.getMod(1, mod1) < m.getMod(2, mod2) {
			m.setMod(3, mod3, 1)
		} else {
			m.setMod(3, mod3, 0)
		}
		m.ip = m.ip + 4
	case OpEQ:
		if m.getMod(1, mod1) == m.getMod(2, mod2) {
			m.setMod(3, mod3, 1)
		} else {
			m.setMod(3, mod3, 0)
		}
		m.ip = m.ip + 4

	case OpSETREL:
		m.relBase = m.relBase + m.getMod(1, mod1)
		m.ip = m.ip + 2
	case OpHALT:
		return false
	}
	return true
}

func (m *Machine) setMod(offset int64, mod int, value int64) {
	switch mod {
	case 1: // immediate
		m.state[m.ip+offset] = value
	case 2: // relative
		m.state[m.state[m.ip+offset]+m.relBase] = value
	default: // positional
		m.state[m.state[m.ip+offset]] = value
	}
}

func (m *Machine) getMod(offset int64, mod int) int64 {
	switch mod {
	case 1: // immediate
		return m.state[m.ip+offset]
	case 2: // relative
		return m.state[m.state[m.ip+offset]+m.relBase]
	default: // positional
		return m.state[m.state[m.ip+offset]]
	}
}

func parseInstruction(in int64) (op OpCode, mod1, mod2, mod3 int) {
	op = OpCode(in % 100)
	if in >= 100 {
		mod1 = int(in/100) % 10
	}
	if in >= 1000 {
		mod2 = int(in/1000) % 10
	}
	if in >= 10000 {
		mod3 = int(in/10000) % 10
	}
	return
}
