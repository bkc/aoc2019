package main

import (
	"fmt"
	"time"
)

type Direction int

const (
	DirUp    = iota
	DirLeft  = iota
	DirDown  = iota
	DirRight = iota
)

func (d Direction) String() string {
	switch d {
	case DirUp:
		return "up"
	case DirDown:
		return "down"
	case DirLeft:
		return "left"
	default:
		return "right"
	}
}

type Color int64

const (
	ColorBlack = 0
	ColorWhite = 1
)

func (c Color) String() string {
	switch c {
	case ColorWhite:
		return "white"
	default:
		return "black"
	}
}

type Point64 struct {
	x, y  int64
	color Color
}

func absInt64(a int64) int64 {
	if a < 0 {
		return 0 - a
	}
	return a
}

func (p Point64) Square(b Point64) Point64 {
	x := absInt64(p.x - b.x)
	y := absInt64(p.y - b.y)
	return Point64{x: x, y: y}
}

func (p Point64) String() string {
	return fmt.Sprintf("[%d,%d]", p.x, p.y)
}

type Robot struct {
	direction Direction
	pos       Point64
	max       Point64
	min       Point64
	painted   []Point64
	moves     int64
	moving    bool
}

func NewRobot() *Robot {
	return &Robot{direction: DirUp}
}

func (r *Robot) turn(dir int64) {
	if dir == 0 { // left
		switch r.direction {
		case DirUp:
			r.direction = DirLeft
		case DirLeft:
			r.direction = DirDown
		case DirDown:
			r.direction = DirRight
		case DirRight:
			r.direction = DirUp
		}
	}
	if dir == 1 { // right
		switch r.direction {
		case DirUp:
			r.direction = DirRight
		case DirLeft:
			r.direction = DirUp
		case DirDown:
			r.direction = DirLeft
		case DirRight:
			r.direction = DirDown
		}
	}
}

func (r *Robot) TurnAndMove(dir int64) {
	r.moves++

	r.turn(dir)

	switch r.direction {
	case DirUp:
		r.pos.y--
	case DirDown:
		r.pos.y++
	case DirLeft:
		r.pos.x--
	case DirRight:
		r.pos.x++
	}

	r.calcMinMax()

	fmt.Println("moved:", r.pos, Direction(r.direction))

	r.moving = false
}

func (r *Robot) calcMinMax() {
	if r.pos.x > r.max.x {
		r.max.x = r.pos.x
	}
	if r.pos.y > r.max.y {
		r.max.y = r.pos.y
	}
	if r.pos.x < r.min.x {
		r.min.x = r.pos.x
	}
	if r.pos.y < r.min.y {
		r.min.y = r.pos.y
	}
}

func (r *Robot) Run(input <-chan int64, done <-chan interface{}) {
	for {
		select {
		case <-done:
			break
		case color := <-input:
			r.moving = true
			r.Paint(Color(color))
			r.TurnAndMove(<-input)
		}
	}
}

func (r *Robot) CameraOutput() int64 {
	for r.moving {
		time.Sleep(1 * time.Nanosecond)
	}
	for _, plate := range r.painted {
		if plate.x == r.pos.x && plate.x == r.pos.x {
			// Already painted this plate
			return int64(plate.color)
		}
	}
	return int64(ColorBlack)
}

func (r *Robot) Paint(color Color) {
	for _, plate := range r.painted {
		if plate.x == r.pos.x && plate.x == r.pos.x {
			// Already painted this plate
			return
		}
	}
	r.painted = append(r.painted, Point64{x: r.pos.x, y: r.pos.y, color: Color(color)})
	// fmt.Println("painted", r.pos, Color(color))
}

func (r *Robot) Painted() int {
	return len(r.painted)
}

func main() {
	machine := NewMachine(puzzle)
	robot := NewRobot()

	done := make(chan interface{})

	go robot.Run(machine.Output(), done)

	camOut := func() int64 {
		out := robot.CameraOutput()
		fmt.Println("pos", robot.pos, "camera:", Color(out))
		return out
	}

	machine.Run(camOut)
	done <- nil

	fmt.Println("painted:", robot.Painted())
	fmt.Println("moved:", robot.moves)
	fmt.Println("board:", robot.min, robot.max, robot.min.Square(robot.max))
}

var puzzle = []int64{3, 8, 1005, 8, 351, 1106, 0, 11, 0, 0, 0, 104, 1, 104, 0, 3, 8, 102, -1, 8, 10, 1001, 10, 1, 10, 4, 10, 108, 1, 8, 10, 4, 10, 102, 1, 8, 28, 3, 8, 1002, 8, -1, 10, 101, 1, 10, 10, 4, 10, 1008, 8, 0, 10, 4, 10, 1002, 8, 1, 51, 1006, 0, 85, 2, 1109, 8, 10, 3, 8, 1002, 8, -1, 10, 101, 1, 10, 10, 4, 10, 1008, 8, 0, 10, 4, 10, 102, 1, 8, 80, 1, 2, 2, 10, 1, 1007, 19, 10, 1, 1001, 13, 10, 3, 8, 1002, 8, -1, 10, 1001, 10, 1, 10, 4, 10, 108, 1, 8, 10, 4, 10, 1001, 8, 0, 113, 1, 2, 1, 10, 1, 1109, 17, 10, 1, 108, 20, 10, 2, 1005, 3, 10, 3, 8, 102, -1, 8, 10, 1001, 10, 1, 10, 4, 10, 108, 1, 8, 10, 4, 10, 1002, 8, 1, 151, 2, 5, 19, 10, 1, 104, 19, 10, 1, 109, 3, 10, 1006, 0, 78, 3, 8, 102, -1, 8, 10, 1001, 10, 1, 10, 4, 10, 1008, 8, 0, 10, 4, 10, 1002, 8, 1, 189, 1006, 0, 3, 2, 1004, 1, 10, 3, 8, 1002, 8, -1, 10, 101, 1, 10, 10, 4, 10, 1008, 8, 1, 10, 4, 10, 1001, 8, 0, 218, 1, 1008, 6, 10, 1, 104, 8, 10, 1006, 0, 13, 3, 8, 1002, 8, -1, 10, 101, 1, 10, 10, 4, 10, 1008, 8, 0, 10, 4, 10, 102, 1, 8, 251, 1006, 0, 17, 1006, 0, 34, 1006, 0, 24, 1006, 0, 4, 3, 8, 102, -1, 8, 10, 1001, 10, 1, 10, 4, 10, 1008, 8, 0, 10, 4, 10, 102, 1, 8, 285, 1006, 0, 25, 2, 1103, 11, 10, 1006, 0, 75, 3, 8, 1002, 8, -1, 10, 1001, 10, 1, 10, 4, 10, 108, 1, 8, 10, 4, 10, 101, 0, 8, 316, 2, 1002, 6, 10, 1006, 0, 30, 2, 106, 11, 10, 1006, 0, 21, 101, 1, 9, 9, 1007, 9, 1072, 10, 1005, 10, 15, 99, 109, 673, 104, 0, 104, 1, 21101, 0, 937151009684, 1, 21101, 0, 368, 0, 1105, 1, 472, 21102, 386979963796, 1, 1, 21102, 379, 1, 0, 1106, 0, 472, 3, 10, 104, 0, 104, 1, 3, 10, 104, 0, 104, 0, 3, 10, 104, 0, 104, 1, 3, 10, 104, 0, 104, 1, 3, 10, 104, 0, 104, 0, 3, 10, 104, 0, 104, 1, 21101, 179410325723, 0, 1, 21101, 426, 0, 0, 1106, 0, 472, 21101, 0, 179355823195, 1, 21102, 437, 1, 0, 1106, 0, 472, 3, 10, 104, 0, 104, 0, 3, 10, 104, 0, 104, 0, 21101, 0, 825460785920, 1, 21101, 460, 0, 0, 1105, 1, 472, 21102, 1, 838429614848, 1, 21102, 1, 471, 0, 1105, 1, 472, 99, 109, 2, 21202, -1, 1, 1, 21102, 40, 1, 2, 21102, 1, 503, 3, 21101, 493, 0, 0, 1105, 1, 536, 109, -2, 2106, 0, 0, 0, 1, 0, 0, 1, 109, 2, 3, 10, 204, -1, 1001, 498, 499, 514, 4, 0, 1001, 498, 1, 498, 108, 4, 498, 10, 1006, 10, 530, 1101, 0, 0, 498, 109, -2, 2106, 0, 0, 0, 109, 4, 2101, 0, -1, 535, 1207, -3, 0, 10, 1006, 10, 553, 21101, 0, 0, -3, 21202, -3, 1, 1, 22101, 0, -2, 2, 21101, 0, 1, 3, 21101, 572, 0, 0, 1105, 1, 577, 109, -4, 2105, 1, 0, 109, 5, 1207, -3, 1, 10, 1006, 10, 600, 2207, -4, -2, 10, 1006, 10, 600, 21202, -4, 1, -4, 1106, 0, 668, 21202, -4, 1, 1, 21201, -3, -1, 2, 21202, -2, 2, 3, 21102, 619, 1, 0, 1105, 1, 577, 22102, 1, 1, -4, 21101, 0, 1, -1, 2207, -4, -2, 10, 1006, 10, 638, 21101, 0, 0, -1, 22202, -2, -1, -2, 2107, 0, -3, 10, 1006, 10, 660, 22101, 0, -1, 1, 21101, 660, 0, 0, 106, 0, 535, 21202, -2, -1, -2, 22201, -4, -2, -4, 109, -5, 2105, 1, 0}
