package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	totalFuel := getInputFuel("input.lst")

	fmt.Println(totalFuel)
}

func getInputFuel(file string) int {
	fp, err := os.Open(file)
	checkErr(err)
	defer fp.Close()
	r := bufio.NewReader(fp)

	var ret int

	line, err := r.ReadString('\n')
	for err == nil {
		mass, err2 := strconv.Atoi(line[:len(line)-1])
		checkErr(err2)
		lastFuel := calcFuel(mass)
		for true {
			ret = ret + lastFuel
			lastFuel = calcFuel(lastFuel)
			if lastFuel <= 0 {
				break
			}
		}
		line, err = r.ReadString('\n')
	}

	return ret
}

func calcFuel(in int) int {
	return in/3 - 2
}
