package main

import (
	// "bytes"
	"fmt"
	"io"
	"testing"
)

func TestInPlane(t *testing.T) {
	tests := []struct {
		a, b, c Asteroid
		ret     bool
	}{
		{
			a:   Asteroid{x: 0, y: 0},
			b:   Asteroid{x: 9, y: 9},
			c:   Asteroid{x: 4, y: 4},
			ret: true,
		},
		{
			a:   Asteroid{x: 0, y: 0},
			b:   Asteroid{x: 0, y: 9},
			c:   Asteroid{x: 0, y: 5},
			ret: true,
		},
		{
			a:   Asteroid{y: 0, x: 0},
			b:   Asteroid{y: 0, x: 9},
			c:   Asteroid{y: 0, x: 5},
			ret: true,
		},
		{
			a:   Asteroid{x: 0, y: 9},
			b:   Asteroid{x: 0, y: 0},
			c:   Asteroid{x: 0, y: 5},
			ret: true,
		},
		{
			a:   Asteroid{y: 0, x: 9},
			b:   Asteroid{y: 0, x: 0},
			c:   Asteroid{y: 0, x: 5},
			ret: true,
		},
		{
			a:   Asteroid{y: 0, x: 0},
			b:   Asteroid{y: 0, x: 9},
			c:   Asteroid{y: 1, x: 5},
			ret: false,
		},
	}

	for i, tt := range tests {
		t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
			r := inPlane(tt.a, tt.b, tt.c)
			if r == true && tt.ret != true {
				t.Errorf("got true, want false")
			}
			if r != true && tt.ret == true {
				t.Errorf("got false, want true")
			}
		})
	}
}

func TestIntersect(t *testing.T) {
	tests := []struct {
		a, b, c Asteroid
		ret     bool
	}{
		{
			a:   Asteroid{x: 0, y: 0},
			b:   Asteroid{x: 9, y: 9},
			c:   Asteroid{x: 4, y: 4},
			ret: true,
		},
		{
			a:   Asteroid{x: 0, y: 0},
			b:   Asteroid{x: 8, y: 4},
			c:   Asteroid{x: 4, y: 2},
			ret: true,
		},
		{
			a:   Asteroid{x: 8, y: 4},
			b:   Asteroid{x: 0, y: 0},
			c:   Asteroid{x: 4, y: 2},
			ret: true,
		},
		{
			a:   Asteroid{x: 0, y: 0},
			b:   Asteroid{x: 0, y: 9},
			c:   Asteroid{x: 0, y: 5},
			ret: true,
		},
		{
			a:   Asteroid{y: 0, x: 0},
			b:   Asteroid{y: 0, x: 9},
			c:   Asteroid{y: 0, x: 5},
			ret: true,
		},
		{
			a:   Asteroid{x: 0, y: 9},
			b:   Asteroid{x: 0, y: 0},
			c:   Asteroid{x: 0, y: 5},
			ret: true,
		},
		{
			a:   Asteroid{y: 0, x: 9},
			b:   Asteroid{y: 0, x: 0},
			c:   Asteroid{y: 0, x: 5},
			ret: true,
		},
		{
			a:   Asteroid{y: 0, x: 0},
			b:   Asteroid{y: 0, x: 9},
			c:   Asteroid{y: 1, x: 5},
			ret: false,
		},
	}

	for i, tt := range tests {
		t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
			r := intersect(tt.a, tt.b, tt.c)
			if r == true && tt.ret != true {
				t.Errorf("got true, want false")
			}
			if r != true && tt.ret == true {
				t.Errorf("got false, want true")
			}
		})
	}
}

func TestAsteroids_rayTrace(t *testing.T) {
	t.Skip()
}

func TestAsteroids_Vaporize(t *testing.T) {
	tests := []struct {
		name      string
		input     io.Reader
		pX, pY    int
		positions []struct {
			pos, x, y int
		}
	}{
		{
			name:  "test 5",
			input: testInput5,
			pX:    11,
			pY:    13,
			positions: []struct{ pos, x, y int }{
				{1, 11, 12},
				{2, 12, 1},
				{3, 12, 2},
				{10, 12, 8},
				{20, 16, 0},
				{50, 16, 9},
				{100, 10, 16},
				{199, 9, 6},
				{200, 8, 2},
				{201, 10, 9},
				{299, 11, 1},
			},
		},
		{
			name:  "test 6",
			input: testInput6,
			pX:    8,
			pY:    3,
			positions: []struct{ pos, x, y int }{
				{1, 8, 1},
				{2, 9, 0},
				{3, 9, 1},
				{4, 10, 0},
				{5, 9, 2},
				{6, 11, 1},
				{7, 12, 1},
				{8, 11, 2},
				{9, 15, 1},
				{10, 12, 2}, // 1
				{11, 13, 2}, // 2
				{12, 14, 2}, // 3
				{13, 15, 2}, // 4
				{14, 12, 3}, // 5
				{15, 16, 4}, // 6
				{16, 15, 4}, // 7
				{17, 10, 4}, // 8
				{18, 4, 4},  // 9
				{19, 2, 4},  // 1
				{20, 2, 3},  // 2
				{21, 0, 2},  // 3
				{22, 1, 2},  // 4
				{23, 0, 1},  // 5
				{24, 1, 1},  // 6
				{25, 5, 2},  // 7
				{26, 1, 0},  // 8
				{27, 5, 1},  // 9
				{28, 6, 1},  // 1
				{29, 6, 0},  // 2
				{30, 7, 0},  // 3
				{31, 8, 0},  // 4
				{32, 10, 1}, // 5
				{33, 14, 0}, // 6
				{34, 16, 1}, // 7
				{35, 13, 3}, // 8
				{36, 14, 3}, // 9
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			belt := parseBelt(tt.input)
			_, gunner := belt.GetPosition(tt.pX, tt.pY)
			vaporized := belt.Vaporize(gunner)

			if len(belt) != len(vaporized)+1 {
				t.Errorf("invalid size: want %d, got %d", len(belt), len(vaporized)+1)
			}

			for _, pos := range tt.positions {
				t.Run(fmt.Sprintf("%d", pos.pos), func(t *testing.T) {
					a := vaporized[pos.pos-1]
					if pos.x != a.x || pos.y != a.y {
						t.Errorf("position is wrong. want [%d,%d], got [%d,%d]", pos.x, pos.y, a.x, a.y)
						i, _ := vaporized.GetPosition(pos.x, pos.y)
						t.Errorf("real one is at pos %d, not %d", i+1, pos.pos)
					}
				})
			}
		})
	}
}

func TestAsteroids(t *testing.T) {
	tests := []struct {
		name      string
		input     io.Reader
		x, y, los int
	}{
		{
			name:  "test input 1",
			input: testInput1,
			x:     3,
			y:     4,
			los:   8,
		},

		{
			name:  "test input 2",
			input: testInput2,
			x:     5,
			y:     8,
			los:   33,
		},
		{
			name:  "test input 3",
			input: testInput3,
			x:     1,
			y:     2,
			los:   35,
		},
		{
			name:  "test input 4",
			input: testInput4,
			x:     6,
			y:     3,
			los:   41,
		},
		{
			name:  "test input 5",
			input: testInput5,
			x:     11,
			y:     13,
			los:   210,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			asteroids := parseBelt(tt.input)
			asteroids.CalcSights()
			a := asteroids.BestPos()
			if a.x != tt.x {
				t.Errorf("wrong x. want %d, got %d", tt.x, a.x)
			}
			if a.y != tt.y {
				t.Errorf("wrong y. want %d, got %d", tt.y, a.y)
			}
			if a.lineOfSight != tt.los {
				t.Errorf("wrong lineOfsight. want %d, got %d", tt.los, a.lineOfSight)
			}
		})
	}
}

func ExampleAsteroids_CalcSights() {
	asteroids := parseBelt(testInput1)
	asteroids.CalcSights()
	asteroids.Print()

	// Output:
	// .7..7
	// .....
	// 67775
	// ....7
	// ...87
}

func (a Asteroids) Size() (w, h int) {
	for _, e := range a {
		if e.x > w {
			w++
		}
		if e.y > h {
			h++
		}
	}
	w++
	h++
	return
}

func (a Asteroids) Print() string {
	w, h := a.Size()

	asteroids := make([]*Asteroid, w*h)

	for _, e := range a {
		asteroids[e.y*w+e.x] = e
	}

	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			ast := asteroids[y*w+x]
			if ast == nil {
				fmt.Printf(".")
			} else {
				fmt.Printf("#")
			}
		}
		fmt.Printf("\n")
	}

	return fmt.Sprint(w, h)
}
