package main

import (
	"fmt"
	"io"
	"math"
	"os"
)

type Asteroid struct {
	lineOfSight, x, y int
}

type Asteroids []*Asteroid

func (ast Asteroids) BestPos() *Asteroid {
	if len(ast) == 0 {
		panic("no asteroids in belt!")
	}
	a := ast[0]

	for _, e := range ast {
		if e.lineOfSight > a.lineOfSight {
			a = e
		}
	}
	return a
}

func (ast Asteroids) GetPosition(x, y int) (int, *Asteroid) {
	for i, a := range ast {
		if a.x == x && a.y == y {
			return i, a
		}
	}
	return -1, nil
}

const DEGREES = 3600000

func (ast Asteroids) splitByDegree(from *Asteroid) [DEGREES]Asteroids {
	var asteroids [DEGREES]Asteroids

	for _, a := range ast {
		if a == from {
			continue
		}
		deg := int(degreesBetween(from, a) * (DEGREES / 360))
		if deg < 0 {
			deg = deg + DEGREES
		}
		if deg == 0 {
			deg = DEGREES
		}
		// I hate math...
		deg = DEGREES - deg
		asteroids[deg] = append(asteroids[deg], a)
	}
	return asteroids
}

func (ast Asteroids) Vaporize(from *Asteroid) Asteroids {
	var ret Asteroids

	asteroids := ast.splitByDegree(from)

	for i := 0; i < DEGREES; i++ {
		if asteroids[i] == nil {
			continue
		}
		if len(asteroids[i]) == 1 {
			continue
		}
		asteroids[i] = asteroids[i].SortByDistance(*from)
	}

	found := true
	for found {
		found = false
		for deg := 0; deg < DEGREES; deg++ {
			if len(asteroids[deg]) == 0 {
				continue
			}
			// fmt.Println(len(asteroids[deg]))
			ret = append(ret, asteroids[deg][0])
			asteroids[deg] = asteroids[deg][1:]
			found = true
		}
		// fmt.Println()
	}

	return ret
}

func (a Asteroid) DistanceTo(b Asteroid) float64 {
	dX := math.Abs(float64(a.x - b.x))
	dY := math.Abs(float64(a.y - b.y))
	return math.Sqrt(math.Pow(dX, 2.0) + math.Pow(dY, 2.0))
}

func (ast Asteroids) SortByDistance(from Asteroid) Asteroids {
	var ret Asteroids
	var distances []float64
	unsorted := make(map[float64]*Asteroid)

	for _, a := range ast {
		dist := from.DistanceTo(*a)
		unsorted[dist] = a
		distances = append(distances, dist)
	}

	distances = bubbleSortFloat64(distances)

	for _, dist := range distances {
		ret = append(ret, unsorted[dist])
	}

	return ret
}

func bubbleSortFloat64(in []float64) []float64 {
	changed := true
	for changed {
		changed = false
		for i := 0; i < len(in)-1; i++ {
			if in[i] > in[i+1] {
				temp := in[i]
				in[i] = in[i+1]
				in[i+1] = temp
				changed = true
			}
		}
	}
	return in
}

func degreesBetween(a, b *Asteroid) float64 {
	dX := float64(a.x - b.x)
	dY := float64(a.y - b.y)
	return math.Atan2(dX, dY) * (180.0 / math.Pi)
}

func (ast Asteroids) CalcSights() {
	for _, a := range ast {
		for _, b := range ast {
			if a == b {
				continue
			}
			blocked := false
			for _, c := range ast {
				if b == c || a == c {
					continue
				}
				if !inPlane(*a, *b, *c) {
					continue
				}
				if intersect(*a, *b, *c) {
					blocked = true
				}
			}
			if !blocked {
				a.lineOfSight++
			}
		}
	}
}

func inPlane(a, b, c Asteroid) bool {
	var (
		planeX, planeY bool
	)
	if (a.x <= c.x && c.x <= b.x) ||
		(a.x >= c.x && c.x >= b.x) {
		planeX = true
	}
	if (a.y <= c.y && c.y <= b.y) ||
		(a.y >= c.y && c.y >= b.y) {
		planeY = true
	}
	return planeX && planeY
}

func absFloat64(a float64) float64 {
	if a < 0.0 {
		return 0.0 - a
	}
	return a
}

func intersect(a, b, c Asteroid) bool {
	if !inPlane(a, b, c) {
		return false
	}

	if (a.x == b.x && a.x == c.x) ||
		(a.y == b.y && a.y == c.y) {
		return true
	}

	diffBX := absFloat64(float64(a.x - b.x))
	diffBY := absFloat64(float64(a.y - b.y))

	diffCX := absFloat64(float64(b.x - c.x))
	diffCY := absFloat64(float64(b.y - c.y))

	if diffBX/diffCX != diffBY/diffCY {
		return false
	}

	return true
}

func main() {
	fp, err := os.Open("input")
	if err != nil {
		panic(err)
	}
	defer fp.Close()

	_ = fp

	asteroids := parseBelt(fp)

	asteroids.CalcSights()

	// part 1
	bestPos := asteroids.BestPos()
	fmt.Println(bestPos.lineOfSight)

	// part 2
	// _, gunner := asteroids.GetPosition(8, 3)
	vaporized := asteroids.Vaporize(bestPos)

	th200 := vaporized[199]

	fmt.Println(th200.x*100 + th200.y)
}

func parseBelt(r io.Reader) Asteroids {
	var ret Asteroids
	buf := make([]byte, 1)
	var (
		err        error
		n          int
		curX, curY int
	)
	for err == nil {
		if n, err = r.Read(buf); err != nil || n == 0 {
			continue
		}
		switch buf[0] {
		case '#':
			a := &Asteroid{x: curX, y: curY}
			ret = append(ret, a)
			curX++
		case '.':
			curX++
		case '\n':
			if curX > 0 {
				curY++
			}
			curX = 0
		}
	}

	return ret
}
