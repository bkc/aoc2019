package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
)

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	fp, err := os.Open("input.lst")
	checkErr(err)
	defer fp.Close()
	r := bufio.NewReader(fp)

	var total int
	for true {
		line, err := r.ReadString('\n')
		if err == io.EOF {
			break
		}
		checkErr(err)
		mass, err := strconv.Atoi(line[:len(line)-1])
		checkErr(err)
		fuelConsumption := mass/3 - 2
		total = total + fuelConsumption
	}

	fmt.Println(total)
}
