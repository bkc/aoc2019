package main

import (
	"testing"
)

func Test_parseInstruction(t *testing.T) {
	type args struct {
		i int
	}
	tests := []struct {
		name     string
		args     args
		wantOp   int
		wantMod1 int
		wantMod2 int
		wantMod3 int
	}{
		{
			name:   "2",
			args:   args{i: 2},
			wantOp: 2,
		},
		{
			name:     "102",
			args:     args{i: 102},
			wantOp:   2,
			wantMod1: 1,
		},
		{
			name:     "1002",
			args:     args{i: 1002},
			wantOp:   2,
			wantMod2: 1,
		},
		{
			name:     "11002",
			args:     args{i: 11002},
			wantOp:   2,
			wantMod2: 1,
			wantMod3: 1,
		},
		{
			name:     "11102",
			args:     args{i: 11102},
			wantOp:   2,
			wantMod1: 1,
			wantMod2: 1,
			wantMod3: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotOp, gotMod1, gotMod2, gotMod3 := parseInstruction(tt.args.i)
			if gotOp != tt.wantOp {
				t.Errorf("parseInstruction() gotOp = %v, want %v", gotOp, tt.wantOp)
			}
			if gotMod1 != tt.wantMod1 {
				t.Errorf("parseInstruction() gotMod1 = %v, want %v", gotMod1, tt.wantMod1)
			}
			if gotMod2 != tt.wantMod2 {
				t.Errorf("parseInstruction() gotMod2 = %v, want %v", gotMod2, tt.wantMod2)
			}
			if gotMod3 != tt.wantMod3 {
				t.Errorf("parseInstruction() gotMod3 = %v, want %v", gotMod3, tt.wantMod3)
			}
		})
	}
}

func Test_iterState(t *testing.T) {
	type args struct {
		state []int
		ip    int
	}
	tests := []struct {
		name  string
		args  args
		want  int
		want1 bool
	}{
		{
			name:  "eq test",
			args:  args{state: []int{8, 5, 6, 5, 99, 8, 8}},
			want:  4,
			want1: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := iterState(tt.args.state, tt.args.ip)
			if got != tt.want {
				t.Errorf("iterState() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("iterState() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func Test_runMachine(t *testing.T) {
	type args struct {
		in []int
	}
	tests := []struct {
		name   string
		inData int
		args   args
		want   int
	}{
		{
			name:   "pos mode - in 7 - out 0",
			inData: 7,
			args:   args{in: []int{3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8}},
			want:   0,
		},
		{
			name:   "pos mode - in 8 - out 1",
			inData: 8,
			args:   args{in: []int{3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8}},
			want:   1,
		},
		{
			name:   "pos mode - in 9 - out 0",
			inData: 9,
			args:   args{in: []int{3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8}},
			want:   0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			inData = tt.inData
			if runMachine(tt.args.in); outData != tt.want {
				t.Errorf("runMachine() = %v, want %v", outData, tt.want)
			}
		})
	}
}
