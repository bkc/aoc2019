package main

import (
	"fmt"
	"os"
)

func main() {
	boost := NewMachine(puzzle)

	in := make(chan int64, 16)
	done := make(chan interface{})

	in <- 2 // test mode...
	go func() {
		boost.Run(in)
		done <- nil
	}()

	for {
		select {
		case i := <-boost.Output():
			fmt.Println(i)
		case <-done:
			os.Exit(0)
		}
	}
}
